import React from 'react';

const AppContext = React.createContext('app_context');

export const AppContextProvider = AppContext.Provider;

export default AppContext;
