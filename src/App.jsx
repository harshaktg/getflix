import React, { useState } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import styled from 'styled-components';
import Home from './components/Home';
import Navbar from './components/Navbar';
import MovieDetail from './components/MovieDetail';
import PageNotFound from './components/PageNotFound';
import { AppContextProvider } from './context';

const AppWrapper = styled.main`
  --primary: #e50914;
  --border-color: #ececec;
  .app-body {
    margin: 5rem 2rem;
    @media (min-width: 481px) and (max-width: 767px) {
      margin: 5rem;
    }
    @media (min-width: 768px) {
      margin: 5rem 10rem;
    }
  }
`;

export default function App() {
  // State to manage search Param
  const [searchParam, setSearchParam] = useState('');
  return (
    <BrowserRouter>
      <AppWrapper>
        <AppContextProvider value={{ searchParam, setSearchParam }}>
          <Navbar />
          <div className="app-body">
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path=":imdbId" element={<MovieDetail />} />
              <Route path="not-found" element={<PageNotFound />} />
            </Routes>
          </div>
        </AppContextProvider>
      </AppWrapper>
    </BrowserRouter>
  );
}
