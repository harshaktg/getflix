import React, {
  useState,
  useRef,
  useCallback,
  useEffect,
  useContext,
} from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import Movie from '../assets/Movie.svg';
import MovieCard from './MovieCard';
import Loader from './Loader';
import { KEY } from '../constants';
import AppContext from '../context';

const IllustrationWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  img {
    height: 40%;
    width: 40%;
  }
`;

const HeaderWrapper = styled.section`
  text-align: center;
  h1 {
    margin: 1rem 0;
    font-size: 2rem;
    color: var(--primary, #ececec);
  }
  p {
    margin-bottom: 2rem;
    opacity: 0.7;
  }
  input {
    padding: 0.5rem;
    border: 2px solid var(--primary, #ececec);
    &:focus {
      outline: none;
    }
    width: 10rem;
    @media (min-width: 481px) and (max-width: 767px) {
      width: 15rem;
    }
    @media (min-width: 768px) {
      width: 20rem;
    }
  }
  button {
    padding: 0.5rem;
    background: var(--primary);
    border: 2px solid var(--primary, #ececec);
    color: white;
    cursor: pointer;
  }
`;

const ContentWrapper = styled.section`
  margin: 4rem 0;
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));
  @media (min-width: 481px) and (max-width: 767px) {
    grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
  }
  @media (min-width: 768px) {
    grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
  }
  grid-gap: 2rem;
  .card {
    border: 1px solid #ececec;
    transition: transform 0.5s;
    -webkit-transition: transform 0.5s;

    .link {
      color: inherit;
      text-decoration: none;
    }
    &:hover {
      cursor: pointer;
      transform: scale(1.1);
      -webkit-transform: scale(1.1);
    }
  }
`;

function Home(props) {
  const context = useContext(AppContext);
  const { searchParam, setSearchParam } = context;
  // const [searchParam, setSearchParam] = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [hasMore, setHasMore] = useState(false);

  const observer = useRef();
  /**
   * Function used to store last element as ref and by using IntersectionObserver API we change page numbers
   */
  const lastMovieRef = useCallback(
    (node) => {
      if (loading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && hasMore) {
          setCurrentPage((prevPage) => prevPage + 1);
        }
      });
      if (node) observer.current.observe(node);
    },
    [loading, hasMore]
  );

  const [movies, setMovies] = useState([]);

  /**
   * Function to fetch list of movies and set respective states
   * @returns undefined
   */
  const fetchData = async () => {
    if (!searchParam.trim()) return;
    setLoading(true);
    const result = await fetch(
      `https://www.omdbapi.com/?apikey=${KEY}&s=${searchParam}&page=${currentPage}`
    );
    const data = await result.json();
    setLoading(false);
    if (data.Response === 'True') {
      setError(null);
      setMovies([...movies, ...data.Search]);
      if (movies.length + data.Search.length < parseFloat(data.totalResults)) {
        setHasMore(true);
      } else {
        setHasMore(false);
      }
    } else {
      setError(data.Error);
      setMovies([]);
    }
  };

  /**
   * Effect to fetch data when page number changes
   */
  useEffect(() => {
    if (currentPage) {
      fetchData();
    } else {
      setCurrentPage(1);
    }
  }, [currentPage]);

  /**
   * Function to reset list and page
   */
  const resetSettings = () => {
    setMovies([]);
    setCurrentPage(0);
  };

  /**
   * Function to reset when submitted
   * @param {*} e Event object
   */
  const handleSubmit = (e) => {
    e.preventDefault();
    resetSettings();
  };

  return (
    <section>
      <HeaderWrapper>
        <header>
          <h1>GetFlix</h1>
        </header>
        <p>GetFlix helps you find the perfect next show or movie to watch.</p>
        <form onSubmit={handleSubmit}>
          <span>
            <input
              type="text"
              placeholder="Search for movies, series, etc"
              value={searchParam}
              disabled={loading}
              onChange={(e) => setSearchParam(e.target.value)}
            />
            <button type="submit" disabled={loading}>
              Search
            </button>
          </span>
        </form>
      </HeaderWrapper>
      <ContentWrapper>
        {movies.map((movie, index) => {
          const { Title, Poster, Type, Year, imdbID } = movie;
          if (movies.length === index + 1) {
            return (
              <div ref={lastMovieRef} key={imdbID} className="card">
                <Link to={`/${imdbID}`} className="link">
                  <MovieCard movie={movie} />
                </Link>
              </div>
            );
          }
          return (
            <div key={imdbID} className="card">
              <Link to={`/${imdbID}`} className="link">
                <MovieCard movie={movie} />
              </Link>
            </div>
          );
        })}
      </ContentWrapper>
      {loading && <Loader />}
      {!movies.length &&
        (error ? (
          <HeaderWrapper>
            <h2>{error}</h2>
          </HeaderWrapper>
        ) : (
          <IllustrationWrapper>
            <img src={Movie} alt="Movie" />
          </IllustrationWrapper>
        ))}
    </section>
  );
}

export default Home;
