import React from 'react';
import NotFound from '../assets/NotFound.svg';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  img {
    width: 50vw;
    height: 50vh;
  }
`;

function PageNotFound() {
  return (
    <Wrapper>
      <img src={NotFound} />
    </Wrapper>
  );
}

export default PageNotFound;
