import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import styled from 'styled-components';
import Search from '../assets/Search.svg';
import { KEY } from '../constants';
import Loader from './Loader';

const ErrorWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  gap: 1rem;
  img {
    height: 50%;
    width: 50%;
  }
`;

const MovieDetailWrapper = styled.div`
  margin: 2rem 0;
  .flex {
    display: flex;
    &.flexi-col {
      margin-bottom: 1rem;
      flex-direction: column;
      @media (min-width: 481px) {
        flex-direction: row;
        margin-bottom: 1rem;
      }
    }
  }
  .a-ctr {
    align-items: flex-start;
    @media (min-width: 481px) {
      align-items: center;
    }
  }
  .jc-btwn {
    justify-content: space-between;
  }
  .movie-info {
    .title {
      font-size: 3rem;
    }
    .type {
      border-radius: 0.4rem;
      padding: 0.4rem;
      border: 1px solid var(--primary, #e50914);
      background-color: #e5091433;
      color: var(--primary, #e50914);
      text-transform: uppercase;
      font-size: 13px;
      font-weight: 600;
    }
    .gap5 {
      gap: 0.5rem;
    }
    .tiny {
      font-size: 12px;
    }
    margin-bottom: 1rem;
    opacity: 0.8;
  }
  .sections {
    display: flex;
    flex-direction: column;
    gap: 10px;
    @media (min-width: 768px) {
      flex-direction: row;
    }
    .left-sec {
      height: 400px;
      width: 300px;
      text-align: center;
      @media (min-width: 481px) {
        text-align: initial;
      }
      img {
        height: 100%;
        object-fit: contain;
        width: inherit;
      }
    }
    .right-sec {
      table {
        .bold {
          font-weight: bold;
        }
        td {
          padding: 0.4rem;
          vertical-align: top;
        }
      }
      .inner-table {
        width: 100%;
        table-layout: fixed;
        border-collapse: collapse;
        overflow-wrap: break-word;
        td {
          text-align: center;
          border: 1px solid var(--border-color, #ececec);
        }
        th {
          font-weight: 600;
          padding: 0.2rem;
          border: 1px solid var(--border-color, #ececec);
        }
      }
    }
  }
`;

function MovieDetail(props) {
  const params = useParams();
  const navigate = useNavigate();

  const [movieData, setMovieData] = useState(null);
  const [loading, setLoading] = useState(null);
  const [error, setError] = useState(null);

  /**
   * Function to fetch movie/series data based on id
   * @param {*} id Id of the particular movie/series
   */
  const fetchData = async (id) => {
    setLoading(true);
    const result = await fetch(
      `https://www.omdbapi.com/?apikey=${KEY}&i=${id}`
    );
    const data = await result.json();
    setLoading(false);
    if (data.Response === 'True') {
      setError(null);
      setMovieData(data);
    } else {
      setError(data.Error);
      setMovieData(null);
      // Redirect user to home page in 5 seconds when error is present
      setTimeout(() => navigate('/'), 5000);
    }
  };

  /**
   * Effect to call fetch data whenever URL param changes
   */
  useEffect(() => {
    if (params.imdbId) {
      fetchData(params.imdbId);
    }
  }, [params]);

  if (loading) {
    return <Loader />;
  }

  if (error) {
    return (
      <ErrorWrapper>
        <h2>{error}</h2>
        <img src={Search} alt="Search" />
        <h4>Redirecting you to home in 5 seconds...</h4>
      </ErrorWrapper>
    );
  }

  if (!movieData) {
    return null;
  }
  const {
    Poster,
    Title,
    Year,
    Rated,
    Runtime,
    imdbRating,
    imdbVotes,
    Ratings,
    Type,
  } = movieData;
  const tableOrder = [
    'Genre',
    'Released',
    'Director',
    'Writer',
    'Actors',
    'Plot',
    'Language',
    'Country',
    'Awards',
  ];
  return (
    <MovieDetailWrapper>
      <div className="movie-info">
        <div className="flex a-ctr gap5 flexi-col">
          <h1 className="title">{Title}</h1>
          <div className="type">{Type}</div>
        </div>
        <div className="flex a-ctr jc-btwn flexi-col">
          <div className="gap5 flex">
            <span>{Year}</span>
            <span>|</span>
            <span>{Rated}</span>
            <span>|</span>
            <span>{Runtime}</span>
          </div>
          <div>
            <p>IMDB Rating</p>
            <h1>{imdbRating}</h1>
            <p className="tiny">{`${imdbVotes} votes`}</p>
          </div>
        </div>
      </div>
      <div className="sections">
        <aside className="left-sec">
          <img
            src={`${
              Poster === 'N/A'
                ? 'https://www.freeiconspng.com/thumbs/no-image-icon/no-image-icon-6.png'
                : Poster
            }`}
          />
        </aside>
        <section className="right-sec">
          <table>
            <tbody>
              {tableOrder.map((item) => (
                <tr>
                  <td className="bold">{item}</td>
                  <td>{movieData[item]}</td>
                </tr>
              ))}
              <tr>
                <td className="bold" rowSpan={2}>
                  Ratings
                </td>
                <td>
                  <table className="inner-table">
                    <thead>
                      <tr>
                        {Ratings.map(({ Source }) => (
                          <th>{Source}</th>
                        ))}
                      </tr>
                    </thead>
                    <tr>
                      {Ratings.map(({ Value }) => (
                        <td>{Value}</td>
                      ))}
                    </tr>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </section>
      </div>
    </MovieDetailWrapper>
  );
}

export default MovieDetail;
