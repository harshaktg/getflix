import React from 'react';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components';

const Wrapper = styled.nav`
  position: fixed;
  height: 3rem;
  background-color: var(--primary);
  top: 0;
  left: 0;
  width: 100%;
  display: flex;
  align-items: center;
  z-index: 1;
  padding: 0 2rem;
  @media (min-width: 481px) and (max-width: 767px) {
    padding: 0 5rem;
  }
  @media (min-width: 768px) {
    padding: 0 10rem;
  }
  .title {
    cursor: pointer;
    color: white;
    font-weight: bold;
  }
`;

function Navbar(props) {
  const navigate = useNavigate();
  return (
    <Wrapper>
      <div className="title" onClick={() => navigate('/')}>
        GetFlix
      </div>
    </Wrapper>
  );
}

export default Navbar;
