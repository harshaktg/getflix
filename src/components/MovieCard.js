import React from 'react';
import styled from 'styled-components';

const MovieCardWrapper = styled.div`
  .poster-holder {
    height: 200px;
    @media (min-width: 481px) and (max-width: 767px) {
      height: 300px;
    }
    @media (min-width: 768px) {
      height: 400px;
    }
    background-repeat: no-repeat;
    background-size: cover;
    &.none {
      background-size: contain;
      background-position: center;
    }
  }
  .card-item {
    padding: 0.5rem;
    .title {
      font-size: 18px;
      font-weight: 700;
    }
    .description {
      opacity: 0.7;
    }
  }
`;

const MovieCard = ({ movie }) => {
  const { Title, Poster, Type, Year, imdbID } = movie;
  return (
    <MovieCardWrapper>
      <div
        className={`poster-holder ${Poster === 'N/A' ? 'none' : ''}`}
        style={{
          backgroundImage: `url(${
            Poster === 'N/A'
              ? 'https://www.freeiconspng.com/thumbs/no-image-icon/no-image-icon-6.png'
              : Poster
          })`,
        }}
      />
      <div className="card-item">
        <h6 className="title">{Title}</h6>
        <p className="description">{Year}</p>
      </div>
    </MovieCardWrapper>
  );
};
export default MovieCard;
